package cts.johnlord.fueltracker;

/**
 * Created by john.lord on 2/23/2018.
 * When implementing with main app, driver info will be pulled from main db.  Code can be modded as needed
 * at that point.
 */


import android.content.ContentValues;
import android.content.Context;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.support.design.widget.Snackbar;
import android.util.Log;
import android.widget.ArrayAdapter;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static android.support.design.widget.Snackbar.LENGTH_INDEFINITE;


public class MyDatabaseHelper extends SQLiteOpenHelper {
    public final static String DATABASE_NAME = "FuelTrackDB";

    private static final int DATABASE_VERSION = 1;

    // Database creation sql statement
    public MyDatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    /**
     * Method is called during creation of database.
     * No checking is required since it only runs during initial install or a db reset.
     * It creates all tables and populates the static lists.
     */
    public void onCreate(SQLiteDatabase database) {  //executed automatically by sqliteopenhelper when database is created.
        //You notice driver and truck are IDs.  This is for integration with main app in the future.
        database.execSQL("create table receipts ( id INTEGER PRIMARY KEY, driverID INTEGER NOT NULL, truckID INTEGER NOT NULL, locationID INTEGER, unitID INTEGER, pricePerUnit REAL, odometer REAL, fillDateUTC STRING, createDateUTC STRING, editDateUTC STRING)");
        database.execSQL("create table locations (id INTEGER PRIMARY KEY, locationName STRING, locationAddress1 STRING, locationAddress2 STRING, locationCity STRING, locationState STRING, locationZip STRING, locationLAT REAL, locationLONG REAL)");
        database.execSQL("create table fuelUnits(id INTEGER PRIMARY KEY, Units STRING)");
        database.execSQL("create table driver(id INTEGER PRIMARY KEY, firstName STRING, lastName STRING, licenseNumber STRING)");
        database.execSQL("create table truck(id INTEGER PRIMARY KEY, make STRING, model STRING, dotNumber STRING)");
        database.execSQL("create table settings(id INTEGER, currentDriverID INTEGER, selectedDriverID INTEGER, currentTruckID INTEGER, selectedTruckID INTEGER, fuelUnitsID INTEGER)");
        ContentValues values = new ContentValues();
        values.put("Units", "US Gallons");
        database.insert("fuelUnits", null, values);
        values.clear();
        values.put("Units", "Litres");
        database.insert("fuelUnits", null, values);
        values.clear();
        values.put("Units", "LP Gas");
        database.insert("fuelUnits", null, values);
        values.clear();
        //intro settings
        values.put("id", 1);
        values.put("currentDriverID", -1);
        values.put("currentTruckID", -1);
        values.put("fuelUnitsID", 0);
        database.insert("settings", null, values);

        values.clear();
        //add more supported units as needed

    }


    public void developerResetDatabase() {
        //used only during testing to reset the database.  Needed if we add or remove fields, etc.
        SQLiteDatabase database;
        database = getWritableDatabase();
        onUpgrade(database, 0, 0);
    }

    public void onUpgrade(SQLiteDatabase database, int oldVersion, int newVersion) {
        Log.w(MyDatabaseHelper.class.getName(),
                "Upgrading database from version " + oldVersion + " to "
                        + newVersion + ", which will destroy all old data");
        database.execSQL("DROP TABLE IF EXISTS receipts");
        database.execSQL("DROP TABLE IF EXISTS locations");
        database.execSQL("DROP TABLE IF EXISTS fuelUnits");
        database.execSQL("DROP TABLE IF EXISTS truck");
        database.execSQL("DROP TABLE IF EXISTS driver");
        database.execSQL("DROP TABLE IF EXISTS settings");

        onCreate(database);
    }

    int getCurrentDriverID() {
        SQLiteDatabase database;
        database = getReadableDatabase();
        Cursor settingsCursor = database.rawQuery("Select * from settings", null);
        settingsCursor.moveToFirst();
        int rtnval = settingsCursor.getCount();
        settingsCursor.close();
        return rtnval;
    }

    /**
     * GetFuelTypeList returns a "LIST" of strings.
     * For simplicity, it simply returns a list of fuel types.
     * When implementing in a drop-down, keep them in order and they will match the db index.
     *
     * @return
     */
    public List<String> getFuelTypeList() {
        List<String> FuelTypes = new ArrayList<String>();

        SQLiteDatabase database;
        database = getReadableDatabase();
        Cursor fuelCursor = database.rawQuery("Select * from fuelUnits", null);
        fuelCursor.moveToFirst();
        while (!(fuelCursor.isAfterLast() == true)) {
            //  FuelTypes.add(fuelCursor.getString(0)); //could be used to alternate index with item
            FuelTypes.add(fuelCursor.getString(1));
            fuelCursor.moveToNext();
        }
        fuelCursor.close();
        return FuelTypes;
    }

    /**
     * Pass this an id and table and it will return that entry.  It doesn't care what table.
     *
     * @param id
     * @param table
     * @return
     */
    public List<String> getRecordByPrimaryKey(long id, String table) {
        //this and truck work exactly the same.  could probably combine them
        List<String> list = new ArrayList<String>();

        SQLiteDatabase database;
        database = getReadableDatabase();
        Cursor cursor = database.rawQuery("Select * from " + table + " where 'id'=" + String.valueOf(id), null);
        if (cursor.getCount() == 0) {
            list.add(String.valueOf(-1));
            cursor.close();
            return list;
        }
        cursor.moveToFirst();
        //start at zero on purpose.  could use the list anywhere so want index kept on it
        for (int cl = 0; cl < cursor.getColumnCount(); cl++) {
            list.add(cursor.getString(cl));
        }

        cursor.close();
        return list;
    }

    boolean DoesTruckExist(String DOT) {
        boolean rtnval;
        SQLiteDatabase database;
        database = getReadableDatabase();
        Cursor settingsCursor = database.rawQuery("Select * from truck where dotNumber='" + DOT + "'", null);
        if (settingsCursor.getCount() == 0) {
            rtnval = false;
        } else {
            rtnval = true;
        }
        settingsCursor.close();
        return rtnval;

    }


    /**
     * This function returns the current primary key of an existing record, or -1 if not exist.
     * This is specifically for the fuel record log.
     *
     * @param driverID
     * @param truckID
     * @return
     */
    int FindExistingFuelRecord(int driverID, int truckID) {
        SQLiteDatabase database;
        database = getReadableDatabase();
        String queryString = "select * from receipts where driverID = '" + String.valueOf(driverID) + "' and truckID = '" + String.valueOf(truckID) + "'";
        Cursor myCursor = database.rawQuery(queryString, null);
        int retValue;
        if (myCursor.getCount() == 0) {
            retValue = -1;
        } else {
            retValue = myCursor.getInt(0);
        }
        myCursor.close();
        return retValue;
    }

    /**
     * @param row   columnn of the item to replace.  there are 4, numbered 0 through 3.  0 is primary key.
     * @param value is the value to replace it with.
     */
    void EditASetting(int row, int value) {
        SQLiteDatabase database;
        database = getWritableDatabase();
        int itm1;
        int itm2;
        int itm3;
        ContentValues values = new ContentValues();

        Cursor myCursor = database.rawQuery("select * from settings", null);
        myCursor.moveToFirst();
        itm1 = myCursor.getInt(1);
        itm2 = myCursor.getInt(2);
        itm3 = myCursor.getInt(3);
        myCursor.close();
        if (row == 1) {
            itm1 = value;
        } else if (row == 2) {
            itm2 = value;
        } else if (row == 3) {
            itm3 = value;
        }

        values.put("currentDriverID", itm1);
        values.put("currentTruckID", itm2);
        values.put("fuelUnitsID", itm3);
        database.update("settings", values, "id=1", null);

        values.clear();

    }

    /**
     * @param primaryKey   if you pass it a primary key, it will try to update the current record
     * @param driverID
     * @param truckID
     * @param locationID
     * @param unitID
     * @param pricePerUnit
     * @param odometer
     * @param fillDateUTC
     * @return
     */

    int addOrUpdateFuelRecord(int primaryKey, int driverID, int truckID, int locationID, int unitID, Double pricePerUnit, Double odometer, String fillDateUTC, String createDateUTC) {

        //adds new fuel record, all at once.  For reference, next line is a copy of the sql it was created with.
        //database.execSQL("create table receipts ( primaryKey INTEGER PRIMARY KEY, driverID INTEGER NOT NULL, truckID INTEGER NOT NULL, locationID INTEGER, unitID INTEGER,
        // pricePerUnit REAL, odometer REAL, fillDateUTC STRING, createDateUTC STRING, editDateUTC STRING)");
        //editDateUTC and createDateUTC are created automatically
        int retVal; //needed so i can close the cursor
        SQLiteDatabase database;
        database = getWritableDatabase();
        String editDateUTC; //auto-generated
        editDateUTC = DateUtility.NowUTC_ISO();
        ContentValues values = new ContentValues();
        values.put("driverID", driverID);
        values.put("truckID", truckID);
        values.put("locationID", locationID);
        values.put("unitID", unitID);
        values.put("pricePerUnit", pricePerUnit);
        values.put("odometer", odometer);
        values.put("fillDateUTC", fillDateUTC);
        values.put("createDateUTC", createDateUTC);
        values.put("editDateUTC", editDateUTC);
        if (primaryKey == -1) {
            retVal = (int) database.insert("receipts", null, values);
        } else {
            //update logic here.  Hard part is locating current record.
            Cursor myCursor = database.rawQuery("select * from receipts where id='" + String.valueOf(primaryKey) + "'", null);
            if (myCursor.getCount() == 0) {
                //assume it was called with update flag on accident.  No match made so create new one
                retVal = (int) database.insert("receipts", null, values);
            } else {
                retVal = (int) database.update("receipts", values, "id='" + String.valueOf(primaryKey) + "'", null);

            }
            myCursor.close();
        }

        return retVal;
    }

    /**
     * @param primaryKey    if not -1, overwrites the current record by primaryKey.  If -1, makes a new one.
     * @param firstName
     * @param lastName
     * @param licenseNumber
     * @return
     */
    long addOrUpdateDriverList(long primaryKey, String firstName, String lastName, String licenseNumber) {
        //declaration:  "create table driver(id INTEGER PRIMARY KEY, firstName STRING, lastName STRING, licenseNumber STRING)");
        long retVal; //needed so i can close the cursor
        SQLiteDatabase database;
        database = getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put("firstName", firstName);
        values.put("lastName", lastName);
        values.put("licenseNumber", licenseNumber);
        if (primaryKey == -1) {
            retVal = database.insert("driver", null, values);
        } else {
            //update logic here.  Hard part is locating current record.
            Cursor myCursor = database.rawQuery("select * from driver where id='" + String.valueOf(primaryKey) + "'", null);
            if (myCursor.getCount() == 0) {
                //assume it was called with update flag on accident.  No match made so create new one
                retVal = database.insert("driver", null, values);
            } else {
                retVal = database.update("driver", values, "id='" + String.valueOf(primaryKey) + "'", null);

            }
            myCursor.close();
        }

        return retVal;
    }

    void fillTruckDataAdapterFromTable( ArrayAdapter adapter){
        SQLiteDatabase database;
        database = getReadableDatabase();

        Cursor myCursor = database.rawQuery("select * from 'truck'", null);
        if (myCursor.getCount() > 0){
            myCursor.moveToFirst();
            for (int cl=0; cl < myCursor.getCount(); cl++){
                myCursor.moveToPosition(cl);

                TruckDataProvider dataProvider = new TruckDataProvider( myCursor.getString(0),  myCursor.getString(1), myCursor.getString(2), myCursor.getString(3));
                adapter.add(dataProvider);
            }
            adapter.notifyDataSetChanged();
        }
        myCursor.close();

    }
    void fillDriverDataAdapterFromTable( ArrayAdapter adapter){
        SQLiteDatabase database;
        database = getReadableDatabase();

        Cursor myCursor = database.rawQuery("select * from 'driver'", null);
        if (myCursor.getCount() > 0){
            myCursor.moveToFirst();
            for (int cl=0; cl < myCursor.getCount(); cl++){
                myCursor.moveToPosition(cl);

                DriverDataProvider dataProvider = new DriverDataProvider( myCursor.getString(0),  myCursor.getString(1), myCursor.getString(2), myCursor.getString(3));
                adapter.add(dataProvider);
            }
            adapter.notifyDataSetChanged();
        }
        myCursor.close();

    }

    long addOrUpdateTruckList(long primaryKey, String make, String model, String dotNumber) {
        //declaration:  "create table driver(id INTEGER PRIMARY KEY, firstName STRING, lastName STRING, licenseNumber STRING)");
        long retVal; //needed so i can close the cursor
        SQLiteDatabase database;
        database = getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put("make", make);
        values.put("model", model);
        values.put("dotNumber", dotNumber);
        if (primaryKey == -1) {
            retVal = database.insert("truck", null, values);
        } else {
            //update logic here.  Hard part is locating current record.
            Cursor myCursor = database.rawQuery("select * from truck where id='" + String.valueOf(primaryKey) + "'", null);
            if (myCursor.getCount() == 0) {
                //assume it was called with update flag on accident.  No match made so create new one
                retVal = database.insert("truck", null, values);
            } else {
                retVal = database.update("truck", values, "id='" + String.valueOf(primaryKey) + "'", null);

            }
            myCursor.close();
        }

        return retVal;
    }
}
