package cts.johnlord.fueltracker;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import static android.widget.Toast.LENGTH_LONG;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link TruckDetails.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link TruckDetails#newInstance} factory method to
 * create an instance of this fragment.
 */
public class TruckDetails extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER

    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    boolean writeIt;
    MyDatabaseHelper dbHelper;
    long index = 1; //will be overwritten if list is clicked
    private OnFragmentInteractionListener mListener;
    private Button button;// = (Button) view.findViewById(R.id.DriverSubmitButton);
    private TextView txtTruckMake;// = (TextView) view.findViewById(R.id.driverFirstName);
    private TextView txtTruckModel;// = (TextView) view.findViewById(R.id.driverLastName);
    private TextView txtTruckDOTNumber;// = (TextView) view.findViewById(R.id.driverLicenseNumber);

    public TruckDetails() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment TruckDetails.
     */
    // TODO: Rename and change types and number of parameters
    public static TruckDetails newInstance(String param1, String param2) {
        TruckDetails fragment = new TruckDetails();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_truck_details, container, false);
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override

    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        dbHelper = new MyDatabaseHelper(getContext()); //MyDatabaseHelpes is a custom class for managing the database

        button = (Button) view.findViewById(R.id.btn);
        txtTruckMake = (TextView) view.findViewById(R.id.truckMake);
        txtTruckModel = (TextView) view.findViewById(R.id.truckModel);
        txtTruckDOTNumber = (TextView) view.findViewById(R.id.truckDOTNum);

        //todo: implement a different way to get truck selection
        if (index != -1) {
            fillInFields();
        }

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                final String make = txtTruckMake.getText().toString();
                final String model = txtTruckModel.getText().toString();
                final String dotNumber = txtTruckDOTNumber.getText().toString();
                //     Toast.makeText(getContext(), "before alertbuilder:" + String.valueOf(index), LENGTH_LONG).show();
                if (index > -1) {
                    AlertDialog.Builder alert = new AlertDialog.Builder(getContext());
                    alert.setTitle("TruckDetails Information");
                    alert.setMessage("Do you wish to overwrite?  You had an entry loaded already.");
                    alert.setIcon(R.drawable.person);
                    alert.setPositiveButton("Overwrite", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int whichButton) {
                            index = dbHelper.addOrUpdateTruckList(index, make, model, dotNumber);
                        }

                    });
                    alert.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int whichButton) {
                            Snackbar.make(getView(), "Changes not saved", Snackbar.LENGTH_INDEFINITE).show();
                        }
                    });
                    alert.setNeutralButton("Create New", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            index = dbHelper.addOrUpdateTruckList(-1, make, model, dotNumber);
                        }
                    });
                    alert.show();

                } else {
                    index = dbHelper.addOrUpdateTruckList(index, make, model, dotNumber);
                }
                Toast.makeText(getContext(), String.valueOf(index), LENGTH_LONG).show();
            }
        });

    }

    void fillInFields() {
        List<String> dString = new ArrayList<String>();
        dString = dbHelper.getRecordByPrimaryKey(index, "truck");
        if (dString.get(0) == "-1") {
            Toast.makeText(getContext(), "You are here(fillinfields)", LENGTH_LONG).show();
            Snackbar snackbar = Snackbar.make(getView(), "No truck found", Snackbar.LENGTH_LONG);
            snackbar.show();
        }

    }
    public void setIndex(int id){
        index = id;
    }

    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
        //     int returnDirverID(int);
    }
}
