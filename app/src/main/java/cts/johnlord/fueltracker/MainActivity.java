package cts.johnlord.fueltracker;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.net.Uri;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;


import java.util.ArrayList;
import java.util.List;


import static android.support.design.widget.Snackbar.*;
import static android.widget.Toast.makeText;

//since i'm not doing any fancy extends, i must implement every single listener for each fragment.
public class MainActivity extends AppCompatActivity   implements
        NavigationView.OnNavigationItemSelectedListener,
        Splash.OnFragmentInteractionListener,
        RefuelList.OnFragmentInteractionListener,
        RefuelDetails.OnFragmentInteractionListener,
        LocationList.OnFragmentInteractionListener,
        LocationDetails.OnFragmentInteractionListener,
        DriverList.OnFragmentInteractionListener,
        DriverDetails.OnFragmentInteractionListener,
        TruckDetails.OnFragmentInteractionListener,
        TruckList.OnFragmentInteractionListener{
    public MyDatabaseHelper dbHelper; //MyDatabaseHelpes is a custom class for managing the database
    FragmentManager fragmentManager = getSupportFragmentManager();

    //instantiate all fragments below this line
    Splash fragmentSplash = new Splash();
    RefuelList fragmentRefuelList = new RefuelList();
    RefuelDetails fragmentRefuelDetails = new RefuelDetails();

    DriverList fragmentDriverList = new DriverList();
    DriverDetails fragmentDriverDetails = new DriverDetails();

    LocationList fragmentLocationsList = new LocationList();
    LocationDetails fragmentLocationsDetails = new LocationDetails();

    TruckDetails fragmentTruckDetails = new TruckDetails();
    TruckList fragmentTruckList = new TruckList();

    FragmentTransaction fragmentTransaction;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

        final MyDatabaseHelper dbHelper = new MyDatabaseHelper(this);
       // final SQLiteDatabase database = dbHelper.getWritableDatabase();

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //get item ids
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fabMain);
        List<String> mStrings = new ArrayList<String>(); //used to get data from dbhelper
        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        fragmentTransaction.add(R.id.fragmentContainer, fragmentSplash); //load initial Splash screen
        fragmentTransaction.commit();
        //set listeners
        navigationView.setNavigationItemSelectedListener(this);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DrawerLayout drawer = (DrawerLayout) findViewById(R.id.main_drawer_layout);
                drawer.openDrawer(GravityCompat.START);
            }
        });

        mStrings = dbHelper.getFuelTypeList();
        dbHelper.addOrUpdateFuelRecord(-1,1,2,3,4, 5.1 ,6.0,"1973/07/28","1973/07/28");
        dbHelper.addOrUpdateFuelRecord(1, 2,2,3,4, 5.1 ,16.0,"1973/07/30","1973/07/29");
    }


    /**
     * This is a callback function that is triggered when a menu item on the side menu is clicked.
     * @param item
     * @return
     */
    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        fragmentTransaction = fragmentManager.beginTransaction();

        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_exit) {

            Toast.makeText(getApplicationContext(),"nav_exit",Toast.LENGTH_LONG).show();
        } else if (id == R.id.nav_locations) {
            AlertDialog.Builder alert = new AlertDialog.Builder(this);
            alert.setTitle("Location Information");
            alert.setMessage("View and choose from fueling station list or enter a new location.");
            alert.setIcon(R.mipmap.ic_map);
            alert.setPositiveButton("View List", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int whichButton) {
                    fragmentTransaction.replace(R.id.fragmentContainer, fragmentLocationsList);
                    fragmentTransaction.addToBackStack(null);
                    fragmentTransaction.commit();


                }
            });
            alert.setNegativeButton("Enter New", new DialogInterface.OnClickListener(){
                public void onClick(DialogInterface dialog, int whichButton) {
                    fragmentTransaction.replace(R.id.fragmentContainer, fragmentLocationsDetails);
                    fragmentTransaction.addToBackStack(null);
                    fragmentTransaction.commit();

                }
            });

            alert.show();
        } else if (id == R.id.nav_truck) {
            AlertDialog.Builder alert = new AlertDialog.Builder(this);
            alert.setTitle("TruckDetails Information");
            alert.setMessage("View and choose from truck list or enter a new truck.");
            alert.setIcon(R.drawable.person);
            alert.setPositiveButton("View List", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int whichButton) {
                    fragmentTransaction.replace(R.id.fragmentContainer, fragmentTruckList);
                    fragmentTransaction.addToBackStack(null);
                    fragmentTransaction.commit();
                }
            });
            alert.setNegativeButton("Enter New", new DialogInterface.OnClickListener(){
                public void onClick(DialogInterface dialog, int whichButton) {
                    fragmentTransaction.replace(R.id.fragmentContainer, fragmentTruckDetails);
                    fragmentTransaction.addToBackStack(null);
                    fragmentTransaction.commit();

                }
            });

            alert.show();
        } else if (id == R.id.nav_driver){
              AlertDialog.Builder alert = new AlertDialog.Builder(this);
              alert.setTitle("DriverDetails Information");
                    alert.setMessage("View and choose from driver list or enter a new driver.");
                    alert.setIcon(R.drawable.person);
                    alert.setPositiveButton("View List", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int whichButton) {
                            fragmentTransaction.replace(R.id.fragmentContainer, fragmentDriverList);
                            fragmentTransaction.addToBackStack(null);
                            fragmentTransaction.commit();

                        }
                    });
                    alert.setNegativeButton("Enter New", new DialogInterface.OnClickListener(){
                        public void onClick(DialogInterface dialog, int whichButton) {
                            fragmentTransaction.replace(R.id.fragmentContainer, fragmentDriverDetails);
                            fragmentTransaction.addToBackStack(null);
                            fragmentTransaction.commit();

                        }
                    });

            alert.show();


        } else if (id == R.id.nav_Refuel) {
            fragmentTransaction.replace(R.id.fragmentContainer, fragmentRefuelDetails);
            fragmentTransaction.addToBackStack(null);
            fragmentTransaction.commit();
        } else if (id == R.id.nav_settings) {
            Toast.makeText(getApplicationContext(),"not implemented", Toast.LENGTH_LONG).show();
        } else if (id == R.id.nav_sync) {
            Toast.makeText(getApplicationContext(),"Place website sync code here", Toast.LENGTH_LONG).show();
        } else if (id == R.id.nav_ViewHistory) {
            fragmentTransaction.replace(R.id.fragmentContainer, fragmentRefuelList);
            fragmentTransaction.addToBackStack(null);
            fragmentTransaction.commit();

        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.main_drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }


    @Override
    public void onFragmentInteraction(Uri uri) {


    }

    @Override
    public void driverItemIndex(String id) {

    }

    @Override
    public void truckItemIndex(String id) {

        int index =  Integer.valueOf(id);
        fragmentTruckDetails.setIndex(index);

        fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.fragmentContainer, fragmentTruckDetails);
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();

    }
}
