package cts.johnlord.fueltracker;
//ripped from kda's utility functions

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

public class DateUtility {
    public static final String TEMPLATE_ISO_FULL = "yyyy-MM-dd'T'HH:mm:ss";
    public final static DateFormat DATEFORMAT_UTC_ISO = GetDateFormat(TEMPLATE_ISO_FULL, "UTC");

    public static Date Now() {
        return new Date(System.currentTimeMillis());
    }

    public static String NowUTC_ISO() {
        return DATEFORMAT_UTC_ISO.format(Now());
    }


    public static final DateFormat GetDateFormat(String template, String timeZone) {
        return GetDateFormat(template, timeZone, false);
    }

    public static final DateFormat GetDateFormat(String template, String timeZone, boolean isLenient) {
        DateFormat ret = new SimpleDateFormat(template, Locale.getDefault());
        // JT#8962 (Handle known SimpleDateFormat issue of extra leading zeros): Set formatter leniency to false (to avoid 2017-0011-28T0020:21:22
        ret.setLenient(isLenient);
        // if timezone is not provided, then just use local (which is already assigned)
        if (timeZone != null) {
            ret.setTimeZone(TimeZone.getTimeZone(timeZone));
        }
        return ret;
    }

}
