package cts.johnlord.fueltracker;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by John on 11/6/2016.
 */

public class DriverDataAdapter extends ArrayAdapter {
//this is the main data adapter for the listview.  It loads data from listDataProvider and it also controls
    //inflating of the views
    List list = new ArrayList();

    public DriverDataAdapter(Context context, int resource) {
        super(context, resource);
    }
    static class DataHandler { //easy object with multiple variables
        TextView txtFirstName;
        TextView txtLastName;
        TextView txtDBKey;
        TextView txtLicenseNumber;
    }

    @Override
    public void add(Object object) {
        super.add(object);
        list.add(object);
    }
    @Override
    public int getCount() {
        return this.list.size();

    }
    @Override
    public Object getItem(int position) {
        return this.list.get(position);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View row;
        row = convertView;
        DataHandler handler;
        if(convertView == null){
            LayoutInflater inflater = (LayoutInflater)this.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            row = inflater.inflate(R.layout.driver_row_layout, parent, false);
            handler = new DataHandler();
            handler.txtFirstName = (TextView)row.findViewById(R.id.txtFirstName);
            handler.txtLastName = (TextView)row.findViewById(R.id.txtLastName);
            handler.txtLicenseNumber = (TextView)row.findViewById(R.id.txtLicenseNumber);
            handler.txtDBKey = (TextView)row.findViewById(R.id.txtDBKey);
            row.setTag(handler);
        }
        else {
            //i barealy understand this.  It seems to store a tag of the correct datahandler so the app knows which one to view.
            //it is necessary due to view recycling.
            handler = (DataHandler) row.getTag();
        }
        DriverDataProvider dataProvider;
        dataProvider = (DriverDataProvider)this.getItem(position);
        handler.txtFirstName.setText(dataProvider.get_FirstName());
        handler.txtLastName.setText(dataProvider.get_LastName());
        handler.txtLicenseNumber.setText(dataProvider.get_LicenseNumber());
        handler.txtDBKey.setText(dataProvider.get_Id());
        return row;
    }
}
