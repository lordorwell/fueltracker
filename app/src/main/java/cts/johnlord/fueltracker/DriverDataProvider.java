package cts.johnlord.fueltracker;


//this is a standard getter/setter for an adapter.

/**
 * Created by John on 11/6/2016.
 */

public class DriverDataProvider {
    //5 used fields in the listview
    private String id;
    private String firstName;
    private String lastName;
    private String licenseNumber;

    //setters
    public void set_Id(String id){this.id = id;}
    public void set_FirstName(String firstName) {
        this.firstName = firstName;
    }
    public void set_LastName(String lastName) { this.lastName = lastName; }
    public void set_LicenseNumber(String licenseNumber) {
        this.licenseNumber = licenseNumber;
    }

   public DriverDataProvider(String id, String firstName, String lastName, String licenseNumber) {
       this.set_Id(id); //id is actually a number in real life
       this.set_FirstName(firstName);
       this.set_LastName(lastName);
       this.set_LicenseNumber(licenseNumber);
   }
    //getters
    public String get_Id() {
        return id;
    }
    public String get_FirstName() { return firstName;}
    public String get_LastName() {return lastName;}
    public String get_LicenseNumber() {
        return licenseNumber;
    }

}
