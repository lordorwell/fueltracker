package cts.johnlord.fueltracker;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import static android.widget.Toast.LENGTH_LONG;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link DriverDetails.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link DriverDetails#newInstance} factory method to
 * create an instance of this fragment.
 */
public class DriverDetails extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER

    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    boolean writeIt;
    MyDatabaseHelper dbHelper;
    long index = 1; //will be overwritten if list is clicked
    private OnFragmentInteractionListener mListener;
    private Button button;// = (Button) view.findViewById(R.id.DriverSubmitButton);
    private TextInputLayout txtFirstNameWrapper;// = (TextView) view.findViewById(R.id.driverFirstName);
    private TextInputLayout txtLastNameWrapper;// = (TextView) view.findViewById(R.id.driverLastName);
    private TextInputLayout txtLicenseNumWrapper;// = (TextView) view.findViewById(R.id.driverLicenseNumber);

    public DriverDetails() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment DriverDetails.
     */
    // TODO: Rename and change types and number of parameters
    public static DriverDetails newInstance(String param1, String param2) {
        DriverDetails fragment = new DriverDetails();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_driver_details, container, false);
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }
    @Override

    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
         dbHelper = new MyDatabaseHelper(getContext()); //MyDatabaseHelpes is a custom class for managing the database

        button = (Button) view.findViewById(R.id.DriverSubmitButton);
        txtFirstNameWrapper = (TextInputLayout) view.findViewById(R.id.driverFirstNameWrapper);
        txtLastNameWrapper = (TextInputLayout) view.findViewById(R.id.driverLastNameWrapper);
        txtLicenseNumWrapper = (TextInputLayout) view.findViewById(R.id.driverLicenseNumWrapper);

        //todo: implement a different way to get driver selection
        if (index != -1){
            fillInFields();
        }

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                final String firstName = txtFirstNameWrapper.getEditText().getText().toString();
                final String lastName = txtLastNameWrapper.getEditText().getText().toString();
                final String licenseNumber = txtLicenseNumWrapper.getEditText().getText().toString();
           //     Toast.makeText(getContext(), "before alertbuilder:" + String.valueOf(index), LENGTH_LONG).show();
                int err=0;
                if (firstName.equals("")){
                    err++;
                    txtFirstNameWrapper.setError("first name is required");
                } else {txtFirstNameWrapper.setErrorEnabled(false);}
                if (lastName.equals("")){
                    err++;
                    txtLastNameWrapper.setError("last name is required");
                } else {txtLastNameWrapper.setErrorEnabled(false);}
                if (licenseNumber.equals("")){
                    err++;
                    txtLicenseNumWrapper.setError("license num is required");
                } else {txtLicenseNumWrapper.setErrorEnabled(false);}
                if (err==0) {
                    if (index > -1) {
                        AlertDialog.Builder alert = new AlertDialog.Builder(getContext());
                        alert.setTitle("DriverDetails Information");
                        alert.setMessage("Do you wish to overwrite?  You had an entry loaded already.");
                        alert.setIcon(R.drawable.person);
                        alert.setPositiveButton("Overwrite", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int whichButton) {
                                index = dbHelper.addOrUpdateDriverList(index, firstName, lastName, licenseNumber);
                            }

                        });
                        alert.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int whichButton) {
                                Snackbar.make(getView(), "Changes not saved", Snackbar.LENGTH_INDEFINITE).show();
                            }
                        });
                        alert.setNeutralButton("Create New", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                index = dbHelper.addOrUpdateDriverList(-1, firstName, lastName, licenseNumber);
                            }
                        });
                        alert.show();

                    } else {
                        index = dbHelper.addOrUpdateDriverList(index, firstName, lastName, licenseNumber);
                    }
                }
                Toast.makeText(getContext(),String.valueOf(index),LENGTH_LONG).show();
            }
        });

    }
    void fillInFields(){
        List<String> dString = new ArrayList<String>();
        dString = dbHelper.getRecordByPrimaryKey(index, "driver");
        if (dString.get(0) == "-1") {
            Toast.makeText(getContext(), "You are here(fillinfields)", LENGTH_LONG).show();
            Snackbar snackbar = Snackbar.make(getView(),"No driver found", Snackbar.LENGTH_LONG);
            snackbar.show();
        }
    }


    public void driverSelected(int driver) {
        index = driver;
    }

    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
   //     int returnDirverID(int);
    }
}
