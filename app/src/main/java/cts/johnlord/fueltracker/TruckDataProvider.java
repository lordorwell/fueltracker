package cts.johnlord.fueltracker;


//this is a standard getter/setter for an adapter.

/**
 * Created by John on 11/6/2016.
 */

public class TruckDataProvider {
    //5 used fields in the listview
    private String id;
    private String make;
    private String model;
    private String dotNumber;

    //setters
    public void set_Id(String id){this.id = id;}
    public void set_Make(String make) {
        this.make = make;
    }
    public void set_Model(String model) { this.model = model; }
    public void set_DotNumber(String dotNumber) {
        this.dotNumber = dotNumber;
    }

   public TruckDataProvider(String id, String make, String model, String dotNumber) {
       this.set_Id(id); //id is actually a number in real life
        this.set_Make(make);
       this.set_Model(model);
       this.set_DotNumber(dotNumber);
   }
    //getters
    public String get_Id() {
        return id;
    }
    public String get_Make() { return make;}
    public String get_Model() {return model;}
    public String get_DotNumber() {
        return dotNumber;
    }

}
