package cts.johnlord.fueltracker;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by John on 11/6/2016.
 */

public class TruckDataAdapter extends ArrayAdapter {
//this is the main data adapter for the listview.  It loads data from listDataProvider and it also controls
    //inflating of the views
    List list = new ArrayList();

    public TruckDataAdapter(Context context, int resource) {
        super(context, resource);
    }
    static class DataHandler { //easy object with multiple variables
        TextView txtModel;
        TextView txtMake;
        TextView txtDBKey;
        TextView txtDOTNumber;
    }

    @Override
    public void add(Object object) {
        super.add(object);
        list.add(object);
    }
    @Override
    public int getCount() {
        return this.list.size();

    }
    @Override
    public Object getItem(int position) {
        return this.list.get(position);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View row;
        row = convertView;
        DataHandler handler;
        if(convertView == null){
            LayoutInflater inflater = (LayoutInflater)this.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            row = inflater.inflate(R.layout.truck_row_layout, parent, false);
            handler = new DataHandler();
            handler.txtMake = (TextView)row.findViewById(R.id.txtMake);
            handler.txtModel = (TextView)row.findViewById(R.id.txtModel);
            handler.txtDOTNumber = (TextView)row.findViewById(R.id.txtDOTNumber);
            handler.txtDBKey = (TextView)row.findViewById(R.id.txtDBKey);
            row.setTag(handler);
        }
        else {
            //i barealy understand this.  It seems to store a tag of the correct datahandler so the app knows which one to view.
            //it is necessary due to view recycling.
            handler = (DataHandler) row.getTag();
        }
        TruckDataProvider dataProvider;
        dataProvider = (TruckDataProvider)this.getItem(position);
        handler.txtMake.setText(dataProvider.get_Make());
        handler.txtModel.setText(dataProvider.get_Model());
        handler.txtDOTNumber.setText(dataProvider.get_DotNumber());
        handler.txtDBKey.setText(dataProvider.get_Id());
        return row;
    }
}
